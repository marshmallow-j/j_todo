import 'package:flutter/material.dart';

class Constants{
  //主页顶部背景色
  static final Color homeTopBackgroundColor = Color(0xffE04658);

  //主页顶部hi字体颜色
  static final Color homeTopTextColor1 = Color(0xffF3BAC1);

  //主页顶部username字体颜色
  static final Color homeTopTextColor2 = Colors.white;

  //头像url
  static final String userAvatar = "https://images.pexels.com/photos/5900524/pexels-photo-5900524.jpeg";

  //chipBorderColor
  static final Color chipBorderColor = Color(0xffE97A86);

  //底部背景色
  static final Color homeBottomBackgroundColor = Color(0xffF4E0DC);

  //阴影颜色
  static final Color shadowColor = Colors.grey[400];


  //默认task_icon
  static final String taskDefaultIcon = "https://images.pexels.com/photos/1856142/pexels-photo-1856142.jpeg";


  //database version
  static final int databaseVersion =  1;
}